import { Directive, ElementRef, Input, HostListener, OnInit } from '@angular/core';

class Color {
  stateId: number;
  color: string;
  constructor(id: number, color: string) {
    this.stateId = id;
    this.color = color;
  }
}
@Directive({
  selector: '[appTaskStatus]'
})
export class TaskStatusDirective implements OnInit {
  @Input('appTaskStatus') stateId: number;

  colors: Color[] = [
    new Color(0, 'orange'),
    new Color(1, 'yellow'),
    new Color(2, '#00FF00')
  ];

  constructor(private el: ElementRef) { }

  ngOnInit() {
    console.log(this.stateId);
    console.log(this.el.nativeElement.style.borderColor = this.colors[this.stateId]);
    this.el.nativeElement.style.borderWidth = '3px';
    this.el.nativeElement.style.borderStyle = 'solid';
    if (this.stateId < 4 && this.stateId >= 0) {
      this.el.nativeElement.style.borderColor = this.colors[this.stateId - 1].color;
    } else {
      this.el.nativeElement.style.borderColor = 'black';
    }
  }
}

// @Directive({
//   selector: '[appTaskStatus]'
// })
// export class TaskStatusDirective {
//   // tslint:disable-next-line: no-input-rename
//   @Input('appTaskStatus') stateId: number;
//   // @Input() stateId: string;

//   colors: Color[] = [
//     new Color(0, 'orange'),
//     new Color(1, 'yellow'),
//     new Color(2, 'green')
//   ];


//   constructor(el: ElementRef) {
//     console.log(this.stateId);
//     el.nativeElement.style.borderWidth = '3px';
//     el.nativeElement.style.borderStyle = 'solid';
//     if (this.stateId < 3 && this.stateId >= 0) {
//       el.nativeElement.style.borderColor = this.colors[this.stateId];
//     }
//     el.nativeElement.style.borderColor = 'black';
//   }

// }
