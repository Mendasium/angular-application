import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'myDatePipe'
})
export class MyDatePipe implements PipeTransform {
  months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  transform(value: string, args?: any): any {
    if (value === undefined) {
     return;
    }
    const date = value.split(/[-T]/, 3);
    const month = this.months[Number.parseInt(date[1], 10) - 1];
    return Number.parseInt(date[2], 10) + ' ' + month + ' ' + date[0];
  }
}
