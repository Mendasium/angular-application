import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyDatePipe } from './my-date.pipe';
import { TaskStatusDirective } from './task-status.directive';

@NgModule({
  declarations: [
    MyDatePipe,
    TaskStatusDirective
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    MyDatePipe,
    TaskStatusDirective
  ]
})
export class SharedModule { }
