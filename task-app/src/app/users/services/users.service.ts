import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from 'src/app/model/model';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient) { }

  getById(id: number) {
    return this.http.get<User>('/api/user/' + id);
  }

  getAllUsers() {
    return this.http.get<User[]>('/api/user');
  }

  createUser(user: User) {
    return this.http.post<User>('/api/user', user);
  }

  updateUser(user: User) {
    return this.http.put('/api/user', user);
  }

  remove(id: number) {
    return this.http.delete('/api/user/' + id);
  }
}
