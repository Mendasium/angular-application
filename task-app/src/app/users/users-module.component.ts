import { Component, OnInit } from '@angular/core';
import { User } from '../model/model';
import { UsersService } from './services/users.service';

@Component({
  selector: 'app-users-module',
  templateUrl: './users-module.component.html',
  styleUrls: ['./users-module.component.css']
})
export class UsersModuleComponent implements OnInit {

  users: User[];

  constructor(private usersService: UsersService) { }

  ngOnInit() {
    console.log('xaxx');
    this.usersService.getAllUsers()
      .subscribe(x => {
        this.users = x;
      });
  }

}
