import { Component, OnInit, Input } from '@angular/core';
import { User } from 'src/app/model/model';
import { UsersService } from '../services/users.service';

@Component({
  selector: 'app-show-all-users',
  templateUrl: './show-all-users.component.html',
  styleUrls: ['./show-all-users.component.css']
})
export class ShowAllUsersComponent implements OnInit {
  users: User[];

  constructor(private userService: UsersService) { }

  ngOnInit() {
    this.userService.getAllUsers()
      .subscribe(x => {
        this.users = x;
      });
  }

}
