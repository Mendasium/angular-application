import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { User } from 'src/app/model/model';
import { UsersService } from '../services/users.service';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { ComponentCanDeactivate } from 'src/app/guards/leaveForm.guard';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit, ComponentCanDeactivate {
  usersForm: FormGroup;
  isCreate = true;
  isOk: boolean = null;
  user: User;

  saved = true;
  save() {
      this.saved = true;
  }

  constructor(private fb: FormBuilder, private userService: UsersService, route: ActivatedRoute) {
    const id = route.snapshot.params.id;
    console.log(new Date(Date.parse('2019-07-31')));
    if (id !== undefined) {
      this.isCreate = false;
      userService.getById(id).subscribe(x => {
        this.user = x;
        this.usersForm.get('firstName').setValue(x.firstName);
        this.usersForm.get('birthday').setValue(x.birthday);
        this.usersForm.get('email').setValue(x.email);
        this.usersForm.get('lastName').setValue(x.lastName);
        this.usersForm.get('teamId').setValue(x.teamId);
      });
    }
  }

  isDate(): boolean {
    const date = new Date(Date.parse(this.usersForm.get('birthday').value));
    console.log(date);
    return date.toString() !== 'Invalid Date';
  }

  ngOnInit() {
   this.initForm();
   this.onChanges();
  }

  onChanges(): void {
    this.usersForm.valueChanges.subscribe(val => {
      this.saved = false;
    });
  }

  initForm() {
   this.usersForm = this.fb.group({
    firstName: ['',
      [
        Validators.required,
        Validators.minLength(3)
      ]],
    birthday: ['',
      [
        Validators.required
      ]],
    email: [''],
    lastName: ['',
      [
        Validators.required,
        Validators.minLength(5)
      ]],
    teamId: ['']
   });
  }

  addUser() {
    if (this.usersForm.invalid) {
      const controls = this.usersForm.controls;
      Object.keys(controls)
        .forEach(controlName => controls[controlName].markAsTouched());
      return;
    }
    if (!this.isDate()) {
      return;
    }

    const user: User = {
      id: 0,
      firstName: this.usersForm.get('firstName').value,
      birthday: new Date(Date.parse(this.usersForm.get('birthday').value)),
      email: this.usersForm.get('email').value,
      lastName: this.usersForm.get('lastName').value,
      teamId: this.usersForm.get('teamId').value > 0 ? this.usersForm.get('teamId').value : 0
    };
    console.log(user);
    if (this.isCreate) {
      this.userService.createUser(user)
      .subscribe(response => {
        if (response.id > 0) {
          this.isOk = true;
        } else {
          this.isOk = false;
        }
      });
    } else {
      user.id = this.user.id;
      this.userService.updateUser(user)
        .subscribe(response => {
          console.log(response);
          this.isOk = true;
      });
    }
    this.save();
  }

  canDeactivate(): boolean | Observable<boolean> {
    if (!this.saved) {
        return confirm('Do you wanna leave this page?');
    } else {
        return true;
    }
  }
}
