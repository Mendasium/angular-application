import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserRoutingModule } from './users-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { UsersService } from './services/users.service';
import { UsersModuleComponent } from './users-module.component';
import { ShowAllUsersComponent } from './show-all-users/show-all-users.component';
import { ShowUserComponent } from './show-user/show-user.component';
import { CreateUserComponent } from './create-user/create-user.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    ShowUserComponent,
    UsersModuleComponent,
    CreateUserComponent,
    ShowAllUsersComponent
  ],
  imports: [
    CommonModule,
    UserRoutingModule,
    ReactiveFormsModule,
    SharedModule
  ],
  providers: [
    UsersService
  ]
})
export class UserModule { }
