import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateUserComponent } from './create-user/create-user.component';
import { ShowAllUsersComponent } from './show-all-users/show-all-users.component';
import { LeaveFormGuard } from '../guards/leaveForm.guard';

const routes: Routes = [
  {
    path: 'users',
    component: ShowAllUsersComponent
  },
  {
    path: 'users/create',
    component: CreateUserComponent,
    canDeactivate: [LeaveFormGuard]
  },
  {
    path: 'users/update/:id',
    component: CreateUserComponent,
    canDeactivate: [LeaveFormGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
