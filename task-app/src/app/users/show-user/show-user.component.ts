import { Component, OnInit, Input } from '@angular/core';
import { User } from 'src/app/model/model';
import { UsersService } from '../services/users.service';

@Component({
  selector: 'app-show-user',
  templateUrl: './show-user.component.html',
  styleUrls: ['./show-user.component.css']
})
export class ShowUserComponent implements OnInit {
  @Input() user: User;
  isRemoved = false;

  constructor(private userService: UsersService) { }

  ngOnInit() {
  }
  remove() {
    this.userService.remove(this.user.id)
      .subscribe(x =>
        this.isRemoved = true
      );
  }
}
