import { Component, OnInit } from '@angular/core';
import { Team } from '../model/model';
import { TeamsService } from './services/teams.service';

@Component({
  selector: 'app-teams-module',
  templateUrl: './teams-module.component.html',
  styleUrls: ['./teams-module.component.css']
})
export class TeamsModuleComponent implements OnInit {

  teams: Team[];

  constructor(private teamsService: TeamsService) { }

  ngOnInit() {
    console.log('xaxx');
    this.teamsService.getAllTeams()
      .subscribe(x => {
        this.teams = x;
      });
  }

}
