import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Team } from 'src/app/model/model';

@Injectable({
  providedIn: 'root'
})
export class TeamsService {

  constructor(private http: HttpClient) { }

  getById(id: number) {
    return this.http.get<Team>('/api/team/' + id);
  }

  getAllTeams() {
    return this.http.get<Team[]>('/api/team');
  }

  createTeam(team: Team) {
    return this.http.post<Team>('/api/team', team);
  }

  updateTeam(team: Team) {
    return this.http.put('/api/team', team);
  }

  remove(id: number) {
    return this.http.delete('/api/team/' + id);
  }
}
