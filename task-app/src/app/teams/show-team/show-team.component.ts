import { Component, OnInit, Input } from '@angular/core';
import { Team } from 'src/app/model/model';
import { TeamsService } from '../services/teams.service';

@Component({
  selector: 'app-show-team',
  templateUrl: './show-team.component.html',
  styleUrls: ['./show-team.component.css']
})
export class ShowTeamComponent implements OnInit {
  @Input() team: Team;
  isRemoved = false;

  constructor(private teamService: TeamsService) { }

  ngOnInit() {
  }
  remove() {
    this.teamService.remove(this.team.id)
      .subscribe(x =>
        this.isRemoved = true
      );
  }
}
