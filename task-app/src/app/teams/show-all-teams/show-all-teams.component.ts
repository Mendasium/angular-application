import { Component, OnInit, Input } from '@angular/core';
import { Team } from 'src/app/model/model';
import { TeamsService } from '../services/teams.service';

@Component({
  selector: 'app-show-all-teams',
  templateUrl: './show-all-teams.component.html',
  styleUrls: ['./show-all-teams.component.css']
})
export class ShowAllTeamsComponent implements OnInit {
  teams: Team[];

  constructor(private teamService: TeamsService) { }

  ngOnInit() {
    this.teamService.getAllTeams()
      .subscribe(x => {
        this.teams = x;
      });
  }

}
