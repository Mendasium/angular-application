import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateTeamComponent } from './create-team/create-team.component';
import { ShowAllTeamsComponent } from './show-all-teams/show-all-teams.component';
import { LeaveFormGuard } from '../guards/leaveForm.guard';

const routes: Routes = [
  {
    path: 'teams',
    component: ShowAllTeamsComponent
  },
  {
    path: 'teams/create',
    component: CreateTeamComponent,
    canDeactivate: [LeaveFormGuard]
  },
  {
    path: 'teams/update/:id',
    component: CreateTeamComponent,
    canDeactivate: [LeaveFormGuard]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TeamRoutingModule { }
