import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Team } from 'src/app/model/model';
import { TeamsService } from '../services/teams.service';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { ComponentCanDeactivate } from 'src/app/guards/leaveForm.guard';

@Component({
  selector: 'app-create-team',
  templateUrl: './create-team.component.html',
  styleUrls: ['./create-team.component.css']
})
export class CreateTeamComponent implements OnInit, ComponentCanDeactivate {
  teamsForm: FormGroup;
  isCreate = true;
  isOk: boolean = null;
  team: Team;

  saved = true;
  save() {
      this.saved = true;
  }

  constructor(private fb: FormBuilder, private teamService: TeamsService, route: ActivatedRoute) {
    const id = route.snapshot.params.id;
    if (id !== undefined) {
      this.isCreate = false;
      teamService.getById(id).subscribe(x => {
        this.team = x;
        this.teamsForm.get('name').setValue(x.name);
      });
    }
  }

  ngOnInit() {
   this.initForm();
   this.onChanges();
  }

  onChanges(): void {
    this.teamsForm.valueChanges.subscribe(val => {
      this.saved = false;
    });
  }

  initForm() {
   this.teamsForm = this.fb.group({
    name: ['',
      [
        Validators.required,
        Validators.minLength(5)
      ]
    ]
   });
  }
  addTeam() {
    if (this.teamsForm.invalid) {
      const controls = this.teamsForm.controls;
      Object.keys(controls)
        .forEach(controlName => controls[controlName].markAsTouched());
      return;
    }
    if (this.isCreate) {
      const team: Team = {
        id: 0,
        name: this.teamsForm.get('name').value
      };
      this.teamService.createTeam(team)
      .subscribe(response => {
        if (response.id > 0) {
          this.isOk = true;
        } else {
          this.isOk = false;
        }
      });
    } else {
      if (this.teamsForm.get('name').value === this.team.name) {
        this.isOk = true;
        return;
      } else {
        this.team.name = this.teamsForm.get('name').value;
        this.teamService.updateTeam(this.team)
          .subscribe(response => {
            console.log(response);
            this.isOk = true;
        });
      }
    }
  }

  canDeactivate(): boolean | Observable<boolean> {
    if (!this.saved) {
        return confirm('Do you wanna leave this page?');
    } else {
        return true;
    }
  }
}
