import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeamsService } from './services/teams.service';
import { ReactiveFormsModule } from '@angular/forms';
import { TeamRoutingModule } from './teams-routing.module';
import { TeamsModuleComponent } from './teams-module.component';
import { ShowAllTeamsComponent } from './show-all-teams/show-all-teams.component';
import { ShowTeamComponent } from './show-team/show-team.component';
import { CreateTeamComponent } from './create-team/create-team.component';

@NgModule({
  declarations: [
    ShowTeamComponent,
    TeamsModuleComponent,
    CreateTeamComponent,
    ShowAllTeamsComponent
  ],
  imports: [
    CommonModule,
    TeamRoutingModule,
    ReactiveFormsModule
  ],
  providers: [
    TeamsService
  ]
})
export class TeamModule { }
