import { Component, OnInit } from '@angular/core';
import { Task } from '../model/model';
import { TasksService } from './services/tasks.service';

@Component({
  selector: 'app-tasks-module',
  templateUrl: './tasks-module.component.html',
  styleUrls: ['./tasks-module.component.css']
})
export class TasksModuleComponent implements OnInit {

  tasks: Task[];

  constructor(private tasksService: TasksService) { }

  ngOnInit() {
    console.log('xaxx');
    this.tasksService.getAllTasks()
      .subscribe(x => {
        this.tasks = x;
      });
  }

}
