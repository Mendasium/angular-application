import { Component, OnInit, Input } from '@angular/core';
import { Task } from 'src/app/model/model';
import { TasksService } from '../services/tasks.service';

@Component({
  selector: 'app-show-all-tasks',
  templateUrl: './show-all-tasks.component.html',
  styleUrls: ['./show-all-tasks.component.css']
})
export class ShowAllTasksComponent implements OnInit {
  tasks: Task[];

  constructor(private taskService: TasksService) { }

  ngOnInit() {
    this.taskService.getAllTasks()
      .subscribe(x => {
        this.tasks = x;
      });
  }

}
