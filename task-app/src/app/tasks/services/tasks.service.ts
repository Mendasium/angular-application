import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Task } from 'src/app/model/model';

@Injectable({
  providedIn: 'root'
})
export class TasksService {

  constructor(private http: HttpClient) { }

  getById(id: number) {
    return this.http.get<Task>('/api/task/' + id);
  }

  getAllTasks() {
    return this.http.get<Task[]>('/api/task');
  }

  createTask(task: Task) {
    return this.http.post<Task>('/api/task', task);
  }

  updateTask(task: Task) {
    return this.http.put('/api/task', task);
  }

  remove(id: number) {
    return this.http.delete('/api/projtaskect/' + id);
  }
}
