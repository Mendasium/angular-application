import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { TasksModuleComponent } from './tasks-module.component';
import { ShowAllTasksComponent } from './show-all-tasks/show-all-tasks.component';
import { TasksRoutingModule } from './tasks-routing.module';
import { TasksService } from './services/tasks.service';
import { ShowTaskComponent } from './show-task/show-task.component';
import { CreateTaskComponent } from './create-task/create-task.component';

@NgModule({
  declarations: [
    ShowTaskComponent,
    TasksModuleComponent,
    CreateTaskComponent,
    ShowAllTasksComponent
  ],
  imports: [
    CommonModule,
    TasksRoutingModule,
    ReactiveFormsModule,
    SharedModule
  ],
  providers: [
    TasksService
  ]
})
export class TasksModule { }
