import { Component, OnInit, Input } from '@angular/core';
import { Task } from 'src/app/model/model';
import { TasksService } from '../services/tasks.service';

@Component({
  selector: 'app-show-task',
  templateUrl: './show-task.component.html',
  styleUrls: ['./show-task.component.css']
})
export class ShowTaskComponent implements OnInit {
  @Input() task: Task;
  isRemoved = false;

  constructor(private taskService: TasksService) { }

  ngOnInit() {
  }
  remove() {
    this.taskService.remove(this.task.id)
      .subscribe(x =>
        this.isRemoved = true
      );
  }
}
