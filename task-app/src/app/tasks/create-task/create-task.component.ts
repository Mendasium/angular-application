import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Task } from 'src/app/model/model';
import { TasksService } from '../services/tasks.service';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { ComponentCanDeactivate } from 'src/app/guards/leaveForm.guard';

@Component({
  selector: 'app-create-task',
  templateUrl: './create-task.component.html',
  styleUrls: ['./create-task.component.css']
})
export class CreateTaskComponent implements OnInit, ComponentCanDeactivate {
  tasksForm: FormGroup;
  isCreate = true;
  isOk: boolean = null;
  task: Task;

  saved = true;
  save() {
      this.saved = true;
  }

  constructor(private fb: FormBuilder, private taskService: TasksService, route: ActivatedRoute) {
    const id = route.snapshot.params.id;
    console.log(new Date(Date.parse('2019-07-31')));
    if (id !== undefined) {
      this.isCreate = false;
      taskService.getById(id).subscribe(x => {
        this.task = x;
        this.tasksForm.get('name').setValue(x.name);
        this.tasksForm.get('description').setValue(x.description);
        this.tasksForm.get('finishedAt').setValue(x.finishedAt);
        this.tasksForm.get('projectId').setValue(x.projectId);
        this.tasksForm.get('stateId').setValue(x.stateId);
        this.tasksForm.get('performerId').setValue(x.performerId);
      });
    }
  }

  isDate(): boolean {
    const date = new Date(Date.parse(this.tasksForm.get('finishedAt').value));
    console.log(date);
    return date.toString() !== 'Invalid Date';
  }

  ngOnInit() {
   this.initForm();
   this.onChanges();
  }

  onChanges(): void {
    this.tasksForm.valueChanges.subscribe(val => {
      this.saved = false;
    });
  }

  initForm() {
   this.tasksForm = this.fb.group({
    name: ['',
      [
        Validators.required,
        Validators.minLength(6)
      ]],
    description: [''],
    finishedAt: ['',
      [
        Validators.required
      ]],
      performerId: ['',
        [
          Validators.required
        ]],
      projectId: ['',
        [
          Validators.required
        ]],
    stateId: ['',
      [
        Validators.required
      ]]
   });
  }

  addTask() {
    if (this.tasksForm.invalid) {
      const controls = this.tasksForm.controls;
      Object.keys(controls)
        .forEach(controlName => controls[controlName].markAsTouched());
      return;
    }
    if (!this.isDate()) {
      return;
    }
    const task: Task = {
      id: 0,
      name: this.tasksForm.get('name').value,
      finishedAt: new Date(Date.parse(this.tasksForm.get('finishedAt').value)),
      description: this.tasksForm.get('description').value,
      performerId: this.tasksForm.get('performerId').value > 0 ? this.tasksForm.get('performerId').value : 0,
      stateId: this.tasksForm.get('stateId').value > 0 ? this.tasksForm.get('stateId').value : 0,
      projectId: this.tasksForm.get('projectId').value > 0 ? this.tasksForm.get('projectId').value : 0
    };
    console.log(task);
    if (this.isCreate) {
      this.taskService.createTask(task)
      .subscribe(response => {
        if (response.id > 0) {
          this.isOk = true;
        } else {
          this.isOk = false;
        }
      });
    } else {
      task.id = this.task.id;
      this.taskService.updateTask(task)
        .subscribe(response => {
          console.log(response);
          this.isOk = true;
      });
    }
    this.save();
  }

  canDeactivate(): boolean | Observable<boolean> {
    if (!this.saved) {
        return confirm('Do you wanna leave this page?');
    } else {
        return true;
    }
  }
}
