import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateTaskComponent } from './create-task/create-task.component';
import { ShowAllTasksComponent } from './show-all-tasks/show-all-tasks.component';
import { LeaveFormGuard } from '../guards/leaveForm.guard';

const routes: Routes = [
  {
    path: 'tasks',
    component: ShowAllTasksComponent
  },
  {
    path: 'tasks/create',
    component: CreateTaskComponent,
    canDeactivate: [LeaveFormGuard]
  },
  {
    path: 'tasks/update/:id',
    component: CreateTaskComponent,
    canDeactivate: [LeaveFormGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TasksRoutingModule { }
