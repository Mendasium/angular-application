import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StatesModuleComponent } from './states-module/states-module.component';

const routes: Routes = [
  // { path: 'states', component: StatesModuleComponent },
  { path: '**', redirectTo: '', data: { stateName: 'legacy' } }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
