import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { State } from 'src/app/model/model';

@Injectable({
  providedIn: 'root'
})
export class StatesService {

  constructor(private http: HttpClient) { }

  getById(id: number) {
    return this.http.get<State>('/api/state/' + id);
  }

  getAllStates() {
    return this.http.get<State[]>('/api/state');
  }

  createState(state: State) {
    return this.http.post<State>('/api/state', state);
  }

  updateState(state: State) {
    return this.http.put('/api/state', state);
  }

  remove(id: number) {
    return this.http.delete('/api/state/' + id);
  }
}
