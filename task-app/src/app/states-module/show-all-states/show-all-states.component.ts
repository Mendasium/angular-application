import { Component, OnInit, Input } from '@angular/core';
import { State } from 'src/app/model/model';
import { StatesService } from '../services/states.service';

@Component({
  selector: 'app-show-all-states',
  templateUrl: './show-all-states.component.html',
  styleUrls: ['./show-all-states.component.css']
})
export class ShowAllStatesComponent implements OnInit {
  states: State[];

  constructor(private statesService: StatesService) { }

  ngOnInit() {
    this.statesService.getAllStates()
      .subscribe(x => {
        this.states = x;
      });
  }

}
