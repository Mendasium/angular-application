import { Component, OnInit } from '@angular/core';
import { State } from '../model/model';
import { StatesService } from './services/states.service';

@Component({
  selector: 'app-states-module',
  templateUrl: './states-module.component.html',
  styleUrls: ['./states-module.component.css']
})
export class StatesModuleComponent implements OnInit {

  states: State[];

  constructor(private statesService: StatesService) { }

  ngOnInit() {
    this.statesService.getAllStates()
      .subscribe(x => {
        this.states = x;
      });
  }

}
