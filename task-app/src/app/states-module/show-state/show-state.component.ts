import { Component, OnInit, Input } from '@angular/core';
import { State } from 'src/app/model/model';
import { StatesService } from '../services/states.service';

@Component({
  selector: 'app-show-state',
  templateUrl: './show-state.component.html',
  styleUrls: ['./show-state.component.css']
})
export class ShowStateComponent implements OnInit {
  @Input() state: State;
  isRemoved = false;

  constructor(private stateService: StatesService) { }

  ngOnInit() {
  }
  remove() {
    this.stateService.remove(this.state.id)
      .subscribe(x =>
        this.isRemoved = true
      );
  }
}
