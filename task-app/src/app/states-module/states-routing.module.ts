import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateStateComponent } from './create-state/create-state.component';
import { ShowAllStatesComponent } from './show-all-states/show-all-states.component';
import { LeaveFormGuard } from '../guards/leaveForm.guard';

const routes: Routes = [
  {
    path: 'states',
    component: ShowAllStatesComponent
  },
  {
    path: 'states/create',
    component: CreateStateComponent,
    canDeactivate: [LeaveFormGuard]
  },
  {
    path: 'states/update/:id',
    component: CreateStateComponent,
    canDeactivate: [LeaveFormGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StatesRoutingModule { }
