import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShowStateComponent } from './show-state/show-state.component';
import { StatesModuleComponent } from './states-module.component';
import { StatesService } from './services/states.service';
import { CreateStateComponent } from './create-state/create-state.component';
import { StatesRoutingModule } from './states-routing.module';
import { ShowAllStatesComponent } from './show-all-states/show-all-states.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ActivatedRouteSnapshot } from '@angular/router';

@NgModule({
  declarations: [
    ShowStateComponent,
    StatesModuleComponent,
    CreateStateComponent,
    ShowAllStatesComponent
  ],
  imports: [
    CommonModule,
    StatesRoutingModule,
    ReactiveFormsModule
  ],
  providers: [
    StatesService
  ]
})
export class StatesModule { }
