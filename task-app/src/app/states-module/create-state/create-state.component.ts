import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { State } from 'src/app/model/model';
import { StatesService } from '../services/states.service';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { ComponentCanDeactivate } from 'src/app/guards/leaveForm.guard';

@Component({
  selector: 'app-create-state',
  templateUrl: './create-state.component.html',
  styleUrls: ['./create-state.component.css']
})
export class CreateStateComponent implements OnInit, ComponentCanDeactivate {
  statesForm: FormGroup;
  isCreate = true;
  isOk: boolean = null;
  state: State;

  saved = true;
  save() {
      this.saved = true;
  }

  constructor(private fb: FormBuilder, private stateService: StatesService, route: ActivatedRoute) {
    const id = route.snapshot.params.id;
    if (id !== undefined) {
      this.isCreate = false;
      stateService.getById(id).subscribe(x => {
        this.state = x;
        this.statesForm.get('value').setValue(x.value);
      });
    }
  }

  ngOnInit() {
   this.initForm();
   this.onChanges();
  }

  onChanges(): void {
    this.statesForm.valueChanges.subscribe(val => {
      this.saved = false;
    });
  }

  initForm() {
   this.statesForm = this.fb.group({
    value: ['',
      [
        Validators.required,
        Validators.minLength(5)
      ]
    ]
   });
  }
  addState() {
    if (this.statesForm.invalid) {
      const controls = this.statesForm.controls;
      Object.keys(controls)
        .forEach(controlName => controls[controlName].markAsTouched());
      return;
    }
    if (this.isCreate) {
      const state: State = {
        id: 0,
        value: this.statesForm.get('value').value
      };
      this.stateService.createState(state)
      .subscribe(response => {
        if (response.id > 0) {
          this.isOk = true;
        } else {
          this.isOk = false;
        }
      });
    } else {
      if (this.statesForm.get('value').value === this.state.value) {
        this.isOk = true;
        return;
      } else {
        this.state.value = this.statesForm.get('value').value;
        this.stateService.updateState(this.state)
          .subscribe(response => {
            console.log(response);
            this.isOk = true;
        });
      }
    }
  }

  canDeactivate(): boolean | Observable<boolean> {
    if (!this.saved) {
        return confirm('Do you wanna leave this page?');
    } else {
        return true;
    }
  }
}
