
export interface State {
    id: number;
    value: string;
}

export interface Team {
    id: number;
    name: string;
}

export interface User {
    id: number;
    firstName: string;
    lastName: string;
    email: string;
    birthday: Date;
    teamId: number;
}

export interface Project {
    id: number;
    name: string;
    description: string;
    deadLine: Date;
    authorId: number;
    teamId: number;
}

export interface Task {
    id: number;
    name: string;
    description: string;
    finishedAt: Date;
    performerId: number;
    stateId: number;
    projectId: number;
}
