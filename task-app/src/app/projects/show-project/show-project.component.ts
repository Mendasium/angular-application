import { Component, OnInit, Input } from '@angular/core';
import { Project } from 'src/app/model/model';
import { ProjectsService } from '../services/projects.service';

@Component({
  selector: 'app-show-project',
  templateUrl: './show-project.component.html',
  styleUrls: ['./show-project.component.css']
})
export class ShowProjectComponent implements OnInit {
  @Input() project: Project;
  isRemoved = false;

  constructor(private projectService: ProjectsService) { }

  ngOnInit() {
  }
  remove() {
    this.projectService.remove(this.project.id)
      .subscribe(x =>
        this.isRemoved = true
      );
  }
}
