import { Component, OnInit, Input } from '@angular/core';
import { Project } from 'src/app/model/model';
import { ProjectsService } from '../services/projects.service';

@Component({
  selector: 'app-show-all-projects',
  templateUrl: './show-all-projects.component.html',
  styleUrls: ['./show-all-projects.component.css']
})
export class ShowAllProjectsComponent implements OnInit {
  projects: Project[];

  constructor(private projectService: ProjectsService) { }

  ngOnInit() {
    this.projectService.getAllProjects()
      .subscribe(x => {
        this.projects = x;
      });
  }

}
