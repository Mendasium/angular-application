import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectRoutingModule } from './projects-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { ProjectsService } from './services/projects.service';
import { ProjectsModuleComponent } from './projects-module.component';
import { ShowAllProjectsComponent } from './show-all-projects/show-all-projects.component';
import { ShowProjectComponent } from './show-project/show-project.component';
import { CreateProjectComponent } from './create-project/create-project.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    ShowProjectComponent,
    ProjectsModuleComponent,
    CreateProjectComponent,
    ShowAllProjectsComponent
  ],
  imports: [
    CommonModule,
    ProjectRoutingModule,
    ReactiveFormsModule,
    SharedModule
  ],
  providers: [
    ProjectsService
  ]
})
export class ProjectsModule { }
