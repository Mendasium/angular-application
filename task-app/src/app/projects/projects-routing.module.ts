import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateProjectComponent } from './create-project/create-project.component';
import { ShowAllProjectsComponent } from './show-all-projects/show-all-projects.component';
import { LeaveFormGuard } from '../guards/leaveForm.guard';

const routes: Routes = [
  {
    path: 'projects',
    component: ShowAllProjectsComponent
  },
  {
    path: 'projects/create',
    component: CreateProjectComponent,
    canDeactivate: [LeaveFormGuard]
  },
  {
    path: 'projects/update/:id',
    component: CreateProjectComponent,
    canDeactivate: [LeaveFormGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectRoutingModule { }
