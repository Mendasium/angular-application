import { Component, OnInit } from '@angular/core';
import { Project } from '../model/model';
import { ProjectsService } from './services/projects.service';

@Component({
  selector: 'app-projects-module',
  templateUrl: './projects-module.component.html',
  styleUrls: ['./projects-module.component.css']
})
export class ProjectsModuleComponent implements OnInit {

  projects: Project[];

  constructor(private projectsService: ProjectsService) { }

  ngOnInit() {
    console.log('xaxx');
    this.projectsService.getAllProjects()
      .subscribe(x => {
        this.projects = x;
      });
  }

}
