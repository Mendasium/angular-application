import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Project } from 'src/app/model/model';

@Injectable({
  providedIn: 'root'
})
export class ProjectsService {

  constructor(private http: HttpClient) { }

  getById(id: number) {
    return this.http.get<Project>('/api/project/' + id);
  }

  getAllProjects() {
    return this.http.get<Project[]>('/api/project');
  }

  createProject(project: Project) {
    return this.http.post<Project>('/api/project', project);
  }

  updateProject(project: Project) {
    return this.http.put('/api/project', project);
  }

  remove(id: number) {
    return this.http.delete('/api/project/' + id);
  }
}
