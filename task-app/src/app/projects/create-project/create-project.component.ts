import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Project } from 'src/app/model/model';
import { ProjectsService } from '../services/projects.service';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { ComponentCanDeactivate } from 'src/app/guards/leaveForm.guard';

@Component({
  selector: 'app-create-project',
  templateUrl: './create-project.component.html',
  styleUrls: ['./create-project.component.css']
})
export class CreateProjectComponent implements OnInit, ComponentCanDeactivate {
  projectsForm: FormGroup;
  isCreate = true;
  isOk: boolean = null;
  project: Project;

  saved = true;
  save() {
      this.saved = true;
  }

  constructor(private fb: FormBuilder, private projectService: ProjectsService, route: ActivatedRoute) {
    const id = route.snapshot.params.id;
    console.log(new Date(Date.parse('2019-07-31')));
    if (id !== undefined) {
      this.isCreate = false;
      projectService.getById(id).subscribe(x => {
        this.project = x;
        this.projectsForm.get('name').setValue(x.name);
        this.projectsForm.get('description').setValue(x.description);
        this.projectsForm.get('deadLine').setValue(x.deadLine);
        this.projectsForm.get('teamId').setValue(x.teamId);
        this.projectsForm.get('authorId').setValue(x.authorId);
      });
    }
  }

  isDate(): boolean {
    const date = new Date(Date.parse(this.projectsForm.get('deadLine').value));
    console.log(date);
    return date.toString() !== 'Invalid Date';
  }

  ngOnInit() {
   this.initForm();
   this.onChanges();
  }

  onChanges(): void {
    this.projectsForm.valueChanges.subscribe(val => {
      this.saved = false;
    });
  }

  initForm() {
   this.projectsForm = this.fb.group({
    name: ['',
      [
        Validators.required,
        Validators.minLength(6)
      ]],
    description: [''],
    deadLine: ['',
      [
        Validators.required
      ]],
    authorId: ['',
      [
        Validators.required
      ]],
    teamId: ['',
      [
        Validators.required
      ]]
   });
  }

  addProject() {
    if (this.projectsForm.invalid) {
      const controls = this.projectsForm.controls;
      Object.keys(controls)
        .forEach(controlName => controls[controlName].markAsTouched());
      return;
    }
    if (!this.isDate()) {
      return;
    }
    const project: Project = {
      id: 0,
      name: this.projectsForm.get('name').value,
      deadLine: new Date(Date.parse(this.projectsForm.get('deadLine').value)),
      description: this.projectsForm.get('description').value,
      authorId: this.projectsForm.get('authorId').value > 0 ? this.projectsForm.get('authorId').value : 0,
      teamId: this.projectsForm.get('teamId').value > 0 ? this.projectsForm.get('teamId').value : 0
    };
    console.log(project);
    if (this.isCreate) {
      this.projectService.createProject(project)
      .subscribe(response => {
        if (response.id > 0) {
          this.isOk = true;
        } else {
          this.isOk = false;
        }
      });
    } else {
      project.id = this.project.id;
      this.projectService.updateProject(project)
        .subscribe(response => {
          console.log(response);
          this.isOk = true;
      });
    }
    this.save();
  }

  canDeactivate(): boolean | Observable<boolean> {
    if (!this.saved) {
        return confirm('Do you wanna leave this page?');
    } else {
        return true;
    }
  }
}
