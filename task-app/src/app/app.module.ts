import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainNavMenuComponent } from './main-nav-menu/main-nav-menu.component';
import { StatesModule } from './states-module/states.module';
import { ApiInterceptor } from './guards/httpInterseptor';
import { HttpClientModule, HttpRequest, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TeamModule } from './teams/team.module';
import { UserModule } from './users/users.module';
import { TasksModule } from './tasks/tasks.module';
import { ProjectsModule } from './projects/projects.module';
import { LeaveFormGuard } from './guards/leaveForm.guard';
import { SharedModule } from './shared/shared.module';

@NgModule({
  declarations: [
    AppComponent,
    MainNavMenuComponent
  ],
  imports: [
    ReactiveFormsModule,
    BrowserModule,
    StatesModule,
    TeamModule,
    UserModule,
    TasksModule,
    ProjectsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    SharedModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiInterceptor,
      multi: true
    },
    LeaveFormGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
