﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using BLL.Services.ServicesAbstraction;
using AutoMapper;
using Model.Model.DTOModel;
using System.Linq;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace TaskProject.Controllers
{
    [Route("api/[controller]")]
    public class StateController : ControllerBase
    {
        IService<DTOState> _statesService;
        IMapper _mapper;
        public StateController(IService<DTOState> statesService, IMapper mapper)
        {
            _mapper = mapper;
            _statesService = statesService;
        }

        [HttpPost]
        public async Task<DTOState> Add([FromBody]DTOState dTOState)
        {
            return await _statesService.Create(dTOState);
        }

        [HttpPut]
        public async Task Update([FromBody]DTOState dTOState)
        {
            await _statesService.Update(dTOState);
        }

        [HttpGet]
        public async Task<IEnumerable<DTOState>> GetAll()
        {
            return await _statesService.GetEntities();
        }

        [HttpGet("{id}")]
        public async Task<DTOState> GetById(int id)
        {
            var res = (await _statesService.GetEntities(id)).FirstOrDefault();
            return res;
        }

        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await _statesService.Delete(id);
        }

        [HttpDelete]
        public async Task Delete(DTOState state)
        {
            await _statesService.Delete(state);
        }
    }
}
