﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Model.Model.DTOModel;
using System.Linq;
using BLL.Services.ServicesAbstraction;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace TaskProject.Controllers
{
    [Route("api/[controller]")]
    public class ProjectController : ControllerBase
    {
        IProjectService _projectsService;
        IMapper _mapper;
        public ProjectController(IProjectService projectsService, IMapper mapper)
        {
            _mapper = mapper;
            _projectsService = projectsService;
        }

        [HttpPost]
        public async Task<DTOProject> Add([FromBody]DTOProject dTOProject)
        {
            return await _projectsService.Create(dTOProject);
        }

        [HttpPut]
        public async Task Update([FromBody]DTOProject dTOProject)
        {
            await _projectsService.Update(dTOProject);
        }

        [HttpGet]
        public async Task<IEnumerable<DTOProject>> GetAll()
        {
            return await _projectsService.GetEntities();
        }

        [HttpGet("{id}")]
        public async Task<DTOProject> GetById(int id)
        {
            return (await _projectsService.GetEntities(id)).FirstOrDefault();
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _projectsService.Delete(id);
        }

        [HttpDelete]
        public void Delete(DTOProject Project)
        {
            _projectsService.Delete(Project);
        }

        [HttpGet("projectTasks/{id}")]
        public async Task<IEnumerable<(DTOProject Project, int TaskCount)>> GetProjectTasksCountByUserId(int id)
        {
            return await _projectsService.GetProjectTasksCountByUserIdAsync(id);
        }

        [HttpGet("userInfo/{id}")]
        public async Task<(DTOUser User, DTOProject LastProject, int CanceledTasksCount, DTOTask LongestTask, int LastProjectUserTasks)> GetUserInfoById(int id)
        {
            return await _projectsService.GetUserInfoById(id);
        }

        [HttpGet("projectInfo/{id}")]
        public async Task<(DTOProject Project, DTOTask LongestTask, DTOTask ShortestTask, int UsersCount)> GetProjectInfoById(int id)
        {
            return await _projectsService.GetProjectInfoById(id);
        }
    }
}
