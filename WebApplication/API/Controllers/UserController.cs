﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using BLL.Services.ServicesAbstraction;
using AutoMapper;
using Model.Model.DTOModel;
using System.Linq;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace TaskProject.Controllers
{
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        IUserService _userService;
        IMapper _mapper;
        public UserController(IUserService userService, IMapper mapper)
        {
            _mapper = mapper;
            _userService = userService;
        }

        [HttpPost]
        public async Task<DTOUser> Add([FromBody]DTOUser dTOUser)
        {
            return await _userService.Create(dTOUser);
        }

        [HttpPut]
        public async void Update([FromBody]DTOUser dTOUser)
        {
            await _userService.Update(dTOUser);
        }

        [HttpGet]
        public async Task<IEnumerable<DTOUser>> GetAll()
        {
            return await _userService.GetEntities();
        }

        [HttpGet("{id}")]
        public async Task<DTOUser> GetById(int id)
        {
            return (await _userService.GetEntities(id)).FirstOrDefault();
        }

        [HttpGet("getSortedTeams")]
        public async Task<IEnumerable<(int id, string name, IEnumerable<DTOUser> userList)>> GetSortedTeams()
        {
            return await _userService.GetSortedTeams();
        }

        [HttpDelete("{id}")]
        public async Task Delete(int id)
        { 
            await _userService.Delete(id);
        }

        [HttpDelete]
        public async Task Delete(DTOUser user)
        {
            await _userService.Delete(user);
        }
    }
}
