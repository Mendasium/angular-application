﻿using AutoMapper;
using Model.Model;
using Model.Model.DTOModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task = Model.Model.Task;

namespace TaskProject.StartupConfiguration
{
    public class MapperConfig
    {
        public static IMapper ConfigureMapping()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<DTOState, State>();
                cfg.CreateMap<State, DTOState>();


                cfg.CreateMap<DTOUser, User>()
                    .ForMember(i => i.TeamId, l => l.MapFrom(i => i.TeamId))
                    .ForMember(i => i.RegisteredAt, opt => opt.Ignore())
                    .ForMember(i => i.Team, opt => opt.Ignore());
                cfg.CreateMap<User, DTOUser>();

                cfg.CreateMap<DTOTeam, Team>()
                    .ForMember(i => i.CreatedAt, opt => opt.Ignore());
                cfg.CreateMap<Team, DTOTeam>();

                cfg.CreateMap<DTOProject, Project>()
                    .ForMember(i => i.TeamId, opt => opt.MapFrom(l => l.TeamId))
                    .ForMember(i => i.AuthorId, opt => opt.MapFrom(l => l.AuthorId))
                    .ForMember(i => i.CreatedAt, opt => opt.Ignore())
                    .ForMember(i => i.Team, opt => opt.Ignore())
                    .ForMember(i => i.Author, opt => opt.Ignore());
                cfg.CreateMap<Project, DTOProject>()
                    .ForMember(i => i.TeamId, opt => opt.MapFrom(l => l.TeamId));

                cfg.CreateMap<DTOTask, Task>()
                    .ForMember(i => i.CreatedAt, l => l.Ignore())
                    .ForMember(i => i.Project, l => l.Ignore())
                    .ForMember(i => i.Performer, l => l.Ignore())
                    .ForMember(i => i.State, l => l.Ignore())
                    .ForMember(i => i.StateId, opt => opt.MapFrom(i => i.StateId));
                cfg.CreateMap<Task, DTOTask>();
            });
            return config.CreateMapper();
        }
    }
}
