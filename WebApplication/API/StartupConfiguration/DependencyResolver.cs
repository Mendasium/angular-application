﻿using DataAccess;
using DataAccess.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Model.Model;
using Model.Model.DTOModel;
using QueueServices.Abstractions;
using QueueServices.Services;
using RabbitMQ.Client;
using SharedServices.Model;
using SharedServices.Writer;
using SharedServices.Writer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using BLL.Services;
using BLL.Services.QueueServices;
using BLL.Services.ServicesAbstraction;

namespace TaskProject.StartupConfiguration
{
    public static class DepencyResolver
    {
        public static void CreateDependencies(this IServiceCollection services, IConfiguration Configuration)
        {
            //Add services and repositories
            services.AddTransient<IRepository<State>, Repository<State>>();
            services.AddTransient<IRepository<User>, Repository<User>>();
            services.AddTransient<IRepository<Team>, Repository<Team>>();
            services.AddTransient<IRepository<Task>, Repository<Task>>();
            services.AddTransient<IRepository<Project>, Repository<Project>>();

            services.AddTransient<IService<DTOState>, StateService>();
            services.AddTransient<IService<DTOUser>, UserService>();
            services.AddTransient<IService<DTOProject>, ProjectService>();
            services.AddTransient<IService<DTOTask>, TaskService>();
            services.AddTransient<IService<DTOTeam>, TeamService>();

            services.AddTransient<IProjectService, ProjectService>();
            services.AddTransient<ITaskService, TaskService>();
            services.AddTransient<IUserService, UserService>();

            //services.AddSingleton<IQueueService, Services.QueueService>();
            services.AddSingleton<IQueueService, EmptyQueueService>();
            services.AddTransient<IMessageService, MessageService>();

            //Add RabbitMQ configuration
            services.AddScoped<IMessageQueue, MessageQueue>();

            services.AddScoped<IMessageProducer, MessageProducer>();
            services.AddScoped<IMessageProducerScoped, MessageProducerScoped>();
            services.AddSingleton<IMessageProducerScopedFactory, MessageProducerScopedFactory>();

            services.AddScoped<IMessageConsumer, MessageConsumer>();
            services.AddScoped<IMessageConsumerScoped, MessageConsumerScoped>();
            services.AddSingleton<IMessageConsumerScopedFactory, MessageConsumerScopedFactory>();



            //Creating connections
            services.AddSingleton<IWriterReader<Message>>(x => new MessageWriterReaderToFile(Configuration.GetSection("MessageLogPath").Value));

            services.AddSingleton<IConnectionFactory>(x => new QueueServices.Services.ConnectionFactory(new Uri(Configuration.GetConnectionString("RabbitMQ"))));

            services.AddDbContext<TaskProjectDBContext>(options => options.UseSqlServer(Configuration.GetConnectionString("LocalDatabaseConnection"), 
                b => b.MigrationsAssembly("TaskProject")));
        }
    }
}
