﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Model.Model;
using Model.Model.DTOModel;
using Task = Model.Model.Task;

namespace BLL.Services.ServicesAbstraction
{
    public interface ITaskService : IService<DTOTask>
    {
        Task<IEnumerable<Task>> GetShortnameTasksByUserId(int id);
        Task<IEnumerable<(int Id, string Name)>> GetFinishedTasksByUserId(int id, int year = 2019);
        Task<IEnumerable<(DTOUser user, IEnumerable<DTOTask> tasks)>> GetSortedUsersWithTasks();
    }
}
