﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Model.Model.DTOModel;

namespace BLL.Services.ServicesAbstraction
{
    public interface IUserService : IService<DTOUser>
    {
        Task<IEnumerable<(int id, string name, IEnumerable<DTOUser> userList)>> GetSortedTeams();
    }
}
