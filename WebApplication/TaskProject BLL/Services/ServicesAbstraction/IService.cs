﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Model.Model.DTOModel;
using Task = System.Threading.Tasks.Task;

namespace BLL.Services.ServicesAbstraction
{
    public interface IService<TEntity> where TEntity : DTOEntity
    {
        Task<IEnumerable<TEntity>> GetEntities(int id = 0, Func<TEntity, bool> filter = null);

        Task<TEntity> Create(TEntity entity);
        Task Update(TEntity entity);
        Task Delete(TEntity entity);
        Task Delete(int id);
    }
}
