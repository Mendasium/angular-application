﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using SharedServices.Model;
using SharedServices.Writer.Interfaces;
using BLL.Services.ServicesAbstraction;

namespace BLL.Services
{
    public class MessageService : IMessageService
    {
        IWriterReader<Message> _writerReader;

        public MessageService(IWriterReader<Message> writerReader)
        {
            _writerReader = writerReader;
        }

        public async Task<IEnumerable<string>> ReadAll()
        {
            return (await _writerReader.ReadAll()).Select(x => x.ToString());
        }
    }
}
