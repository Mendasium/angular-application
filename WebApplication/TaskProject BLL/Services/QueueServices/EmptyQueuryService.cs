﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.Services.ServicesAbstraction;

namespace BLL.Services.QueueServices
{
    public class EmptyQueueService : IQueueService
    {
        public void Send(string message)
        { }
    }
}
