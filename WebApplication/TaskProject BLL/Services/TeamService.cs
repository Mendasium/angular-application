﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Model.Model;
using Model.Model.DTOModel;
using DataAccess.Repositories;
using BLL.Services.ServicesAbstraction;
using Task = System.Threading.Tasks.Task;

namespace BLL.Services
{
    public class TeamService : IService<DTOTeam>
    {
        IRepository<Team> _teams;
        IQueueService _queueService;
        IMapper _mapper;
        CancellationTokenSource token;

        public TeamService(IRepository<Team> teams, IQueueService queueService, IMapper mapper)
        {
            _mapper = mapper;
            _teams = teams;
            _queueService = queueService;
            token = new CancellationTokenSource();
        }

        public async Task<DTOTeam> Create(DTOTeam entity)
        {
            var team = _mapper.Map<DTOTeam, Team>(entity);
            team.CreatedAt = DateTime.Now;
            _teams.Create(team);  
            await _teams.SaveAsync(token.Token);
            _queueService.Send($"Team {team.ToString()} was created");
            
            var result = (await _teams.GetAsync(x => x.Id == team.Id)).First();
            return _mapper.Map<Team, DTOTeam>(result);
        }

        public async Task Delete(DTOTeam entity)
        {
            var team = _mapper.Map<DTOTeam, Team>(entity);
            _teams.Delete(team);
            await _teams.SaveAsync(token.Token);
            _queueService.Send($"Team {team.ToString()} was removed");
        }

        public async Task Delete(int id)
        {
            _teams.Delete(id);
            await _teams.SaveAsync(token.Token);
            _queueService.Send($"Team {id} was removed");
        }

        public async Task<IEnumerable<DTOTeam>> GetEntities(int id = 0, Func<DTOTeam, bool> filter = null)
        {
            _queueService.Send($"Getting all Teams " + (filter == null ? "" : " with filter"));
            if(id > 0)
            {
                return _mapper.Map<IEnumerable<Team>, IEnumerable<DTOTeam>>(await _teams.GetAsync(x => x.Id == id));
            }
            var result = await _teams.GetAsync(_mapper.Map<Func<DTOTeam, bool>, Func<Team, bool>>(filter));
            return _mapper.Map<IEnumerable<Team>, IEnumerable<DTOTeam>>(result);
        }

        public async Task Update(DTOTeam entity)
        {
            var newTeam = _mapper.Map<DTOTeam, Team>(entity);
            var team = (await _teams.GetAsync(x => x.Id == newTeam.Id)).FirstOrDefault();
            if(team != null)
            {
                _teams.Update(newTeam);
                await _teams.SaveAsync(token.Token);
                _queueService.Send($"Team {newTeam.ToString()} was updated");
            }
        }
    }
}
