﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Model.Model;
using Model.Model.DTOModel;
using DataAccess.Repositories;
using BLL.Services.ServicesAbstraction;
using Task = System.Threading.Tasks.Task;

namespace BLL.Services
{
    public class UserService : IUserService
    {
        IRepository<User> _users;
        IRepository<Team> _teams;
        IMapper _mapper;
        private IQueueService _queueService;
        CancellationTokenSource token;

        public UserService(IRepository<User> users, IRepository<Team> teams, IQueueService queueService, IMapper mapper)
        {
            _mapper = mapper;
            _users = users;
            _teams = teams;
            _queueService = queueService;
            token = new CancellationTokenSource();
        }

        public async Task<DTOUser> Create(DTOUser entity)
        {
            var user = _mapper.Map<DTOUser, User>(entity);

            if (user.TeamId != 0)
            {
                user.Team = (await _teams.GetAsync(new Func<Team, bool>(x => x.Id == entity.TeamId))).FirstOrDefault();
            }

            user.RegisteredAt = DateTime.Now;
            user.TeamId = user.Team?.Id;
            _users.Create(user);
            await _users.SaveAsync(token.Token);
            _queueService.Send($"User {user.ToString()} was added");

            return _mapper.Map<User, DTOUser>((await _users.GetAsync(x => x.Id == user.Id)).First());
        }

        public async Task Delete(DTOUser entity)
        {
            var user = _mapper.Map<DTOUser, User>(entity);

            _users.Delete(user);
            await _users.SaveAsync(token.Token);
            _queueService.Send($"User {entity.ToString()} was removed");
        }

        public async Task Delete(int id)
        {
            _users.Delete(id);
            await _users.SaveAsync(token.Token);
            _queueService.Send($"User {id} was removed");
        }

        public async Task<IEnumerable<DTOUser>> GetEntities(int id = 0, Func<DTOUser, bool> filterDTO = null)
        {
            _queueService.Send($"Getting all Users " + (filterDTO == null ? "" : " with filter"));
            if (id > 0)
            {
                return _mapper.Map<IEnumerable<User>, IEnumerable<DTOUser>>(await _users.GetAsync(x => x.Id == id));
            }
            var filter = _mapper.Map<Func<DTOUser, bool>, Func<User, bool>>(filterDTO);
            return _mapper.Map<IEnumerable<User>, IEnumerable<DTOUser>>(await _users.GetAsync(filter));
        }

        public async Task Update(DTOUser entity)
        {
            var user = _mapper.Map<DTOUser, User>(entity);

            var createdUser = (await _users.GetAsync(x => x.Id == user.Id)).FirstOrDefault();
            if (createdUser != null)
            {
                if (user.TeamId != 0)
                {
                    var team = (await _teams.GetAsync(new Func<Team, bool>(x => x.Id == user.TeamId))).FirstOrDefault();
                    if (team != null)
                        user.Team = team;
                }
                _users.Update(user);
                await _users.SaveAsync(token.Token);
                _queueService.Send($"User {user.ToString()} was updated");
            }
        }

        public async Task<IEnumerable<(int id, string name, IEnumerable<DTOUser> userList)>> GetSortedTeams()
        {
            var users = await _users.GetAsync();
            var teams = await _teams.GetAsync();

            return users
                .Where(x => x.TeamId != null)
                .GroupBy(u => u.TeamId)
                .Where(u => u.All(x => DateTime.Now.Year - x.Birthday.Year >= 12))
                .Select(t =>
                    (id: (int)t.Key,
                        name: teams.FirstOrDefault(x => x.Id == t.Key).Name,
                    userList: _mapper.Map<IEnumerable<User>, IEnumerable<DTOUser>>(t.OrderByDescending(x => x.RegisteredAt)))
                    );
        }
    }
}
