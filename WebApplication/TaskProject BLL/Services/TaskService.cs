﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Model.Model;
using Model.Model.DTOModel;
using DataAccess.Repositories;
using BLL.Services.ServicesAbstraction;
using Task = Model.Model.Task;

namespace BLL.Services
{
    public class TaskService : ITaskService
    {
        IRepository<Task> _tasks;
        IRepository<User> _users;
        IRepository<Project> _projects;
        IRepository<State> _states;
        IQueueService _queueService;
        IMapper _mapper;
        CancellationTokenSource token;

        public TaskService(IRepository<Task> tasks, IRepository<User> users, IRepository<Project> projects, IRepository<State> states, IQueueService queueService, IMapper mapper)
        {
            _tasks = tasks;
            _users = users;
            _projects = projects;
            _states = states;
            _mapper = mapper;
            _queueService = queueService;
            token = new CancellationTokenSource();
        }

        public async Task<DTOTask> Create(DTOTask entity)
        {
            var task = _mapper.Map<DTOTask, Task>(entity);

            if (task.ProjectId != 0)
            {
                var project = (await _projects.GetAsync(new Func<Project, bool>(x => x.Id == task.ProjectId))).FirstOrDefault();
                task.Project = project ?? throw new Exception("Such project doesnt exist");
            }
            else
                throw new Exception("Project must be added");

            if (task.PerformerId != 0)
            {
                var performer = (await _users.GetAsync(new Func<User, bool>(x => x.Id == task.PerformerId))).FirstOrDefault();
                task.Performer = performer ?? throw new Exception("Such performer doesnt exist");
            }
            else
                throw new Exception("Performer must be added");
            
            if(task.StateId != 0)
            {
                var state = (await _states.GetAsync(new Func<State, bool>(x => x.Id == task.StateId))).FirstOrDefault();
                task.State = state ?? throw new Exception("Such state doesnt exist");
            }
            else
                throw new Exception("State must be added");

            task.CreatedAt = DateTime.Now;
            _tasks.Create(task);
            await _tasks.SaveAsync(token.Token);
            _queueService.Send($"Task {task.ToString()} was created");

            var result = (await _tasks.GetAsync(x => x.Id == task.Id)).First();

            return _mapper.Map<Task, DTOTask>(result);
        }
        

        public async System.Threading.Tasks.Task Delete(DTOTask entity)
        {
            var task = _mapper.Map<DTOTask, Task>(entity);
            _tasks.Delete(task);
            await _tasks.SaveAsync(token.Token);
            _queueService.Send($"Task {task.ToString()} was removed");
        }

        public async System.Threading.Tasks.Task Delete(int id)
        {
            _tasks.Delete(id);
            await _tasks.SaveAsync(token.Token);
            _queueService.Send($"Task {id} was removed");
        }

        public async Task<IEnumerable<DTOTask>> GetEntities(int id = 0, Func<DTOTask, bool> filter = null)
        {
            _queueService.Send($"Getting all Tasks " + (filter == null ? "" : " with filter"));
            if (id > 0)
            {
                return _mapper.Map<IEnumerable<Task>, IEnumerable<DTOTask>>(await _tasks.GetAsync(x => x.Id == id));
            }
            var result = await _tasks.GetAsync(_mapper.Map<Func<DTOTask, bool>, Func<Task, bool>>(filter));
            return _mapper.Map<IEnumerable<Task>, IEnumerable<DTOTask>>(result);
        }

        public async System.Threading.Tasks.Task Update(DTOTask entity)
        {
            var newTask = _mapper.Map<DTOTask, Task>(entity);

            var task = (await _tasks.GetAsync(x => x.Id == newTask.Id)).FirstOrDefault();
            if (task != null)
            {
                if (newTask.StateId != 0)
                {
                    var state = (await _states.GetAsync(x => x.Id == newTask.StateId)).FirstOrDefault();
                    if (state != null)
                        newTask.State = state;
                }

                _tasks.Update(newTask);
                await _tasks.SaveAsync(token.Token);
                _queueService.Send($"Task {newTask.ToString()} was updated");
            }
        }

        public async Task<IEnumerable<Task>> GetShortnameTasksByUserId(int id)
        {
            var tasks = await _tasks.GetAsync();
            var users = await _users.GetAsync();

            return tasks
                .Join(users, x => x.PerformerId, y => y.Id, (x,y) => new Task()
                {
                    Id = x.Id,
                    CreatedAt = x.CreatedAt,
                    Description = x.Description,
                    ProjectId = x.ProjectId,
                    FinishedAt = x.FinishedAt,
                    Name = x.Name,
                    PerformerId = x.PerformerId,
                    Performer = y,
                    StateId = x.StateId
                })
                .Where(t => t.Name.Length < 45 && t.Performer.Id == id);
        }

        public async Task<IEnumerable<(int Id, string Name)>> GetFinishedTasksByUserId(int id, int year = 2019)
        {
            var tasks = await _tasks.GetAsync();
            var states = await _states.GetAsync();

            var t1 = _tasks.GetAsync();
            var t2 = _states.GetAsync();

            return tasks
                .Join(states, x => x.StateId, y => y.Id, (x,y) => new Task()
                {
                    PerformerId = x.PerformerId,
                    Id = x.Id,
                    Name = x.Name,
                    FinishedAt = x.FinishedAt,
                    State = y
                })
                .Where(t => t.PerformerId == id && t.FinishedAt.Year == year && t.State.Value.Equals("finished"))
                .Select(x => (Id: x.Id, Name: x.Name));
        }

        public async Task<IEnumerable<(DTOUser user, IEnumerable<DTOTask> tasks)>> GetSortedUsersWithTasks()
        {
            var tasks = await _tasks.GetAsync();
            var users = await _users.GetAsync();

            return tasks.GroupBy(x => x.PerformerId)
                .Select(x => (
                        Performer: _mapper.Map<User, DTOUser>(users.FirstOrDefault(y => y.Id == x.Key)),
                        Tasks: _mapper.Map<IEnumerable<Task>, IEnumerable<DTOTask>>(x.OrderByDescending(y => y.Name.Length).ToList())))
                .OrderBy(x => x.Performer.FirstName);
        }
    }
}
