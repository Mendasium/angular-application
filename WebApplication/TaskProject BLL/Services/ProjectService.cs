﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Model.Model;
using Model.Model.DTOModel;
using DataAccess.Repositories;
using BLL.Services.ServicesAbstraction;

namespace BLL.Services
{
    public class ProjectService : IProjectService
    {
        IRepository<Project> _projects;
        IRepository<User> _users;
        IRepository<Team> _teams;
        IRepository<Model.Model.Task> _tasks;
        IRepository<State> _states;
        IQueueService _queueService;
        IMapper _mapper;

        CancellationTokenSource token;

        public ProjectService(IRepository<Project> projects, IRepository<User> users, IRepository<Team> teams, IRepository<Model.Model.Task> tasks, IRepository<State> states, IQueueService queueService, IMapper mapper)
        {
            _states = states;
            _mapper = mapper;
            _tasks = tasks;
            _users = users;
            _projects = projects;
            _teams = teams;
            _queueService = queueService;
            token = new CancellationTokenSource();
        }

        public async Task<DTOProject> Create(DTOProject entity)
        {
            var project = _mapper.Map<DTOProject, Project>(entity);
            if (project.AuthorId == 0)
                throw new Exception("The project can not has no author");
            var author = (await _users.GetAsync(x => x.Id == project.AuthorId)).FirstOrDefault();
            project.Author = author ?? throw new Exception("Such Author doesnt exist");

            if (project.TeamId != 0)
            {
                project.Team = (await _teams.GetAsync(x => x.Id == project.TeamId)).FirstOrDefault();
            }

            project.CreatedAt = DateTime.Now;
            _projects.Create(project);

            await _projects.SaveAsync(token.Token);
            _queueService.Send($"Project {project} was created");
            
            var result = (await _projects.GetAsync(x => x.Id == project.Id)).First();
            return _mapper.Map<Project, DTOProject>(result);
        }

        public async System.Threading.Tasks.Task Delete(DTOProject entity)
        {
            var project = _mapper.Map<DTOProject, Project>(entity);
            _projects.Delete(project);
            await _projects.SaveAsync(token.Token);
            _queueService.Send($"Project {project} was removed");
        }

        public async System.Threading.Tasks.Task Delete(int id)
        {
            _projects.Delete(id);
            await _projects.SaveAsync(token.Token);
            _queueService.Send($"Project {id} was removed");
        }

        public async Task<IEnumerable<DTOProject>> GetEntities(int id = 0, Func<DTOProject, bool> filter = null)
        {
            _queueService.Send($"Getting all Entities " + (filter == null ? "" : " with filter"));
            if (id > 0)
            {
                var res = await _projects.GetAsync(x => x.Id == id);
                return _mapper.Map<IEnumerable<Project>, IEnumerable<DTOProject>>(res);
            }
            var result = await _projects.GetAsync(_mapper.Map<Func<DTOProject, bool>, Func<Project, bool>>(filter));
            return _mapper.Map<IEnumerable<Project>, IEnumerable<DTOProject>>(result);
        }

        public async System.Threading.Tasks.Task Update(DTOProject entity)
        {
            var newProject = _mapper.Map<DTOProject, Project>(entity);


            var project = (await _projects.GetAsync(x => x.Id == newProject.Id)).FirstOrDefault();
            if (project != null)
            {
                if (newProject.TeamId != 0)
                {
                    var team = (await _teams.GetAsync(new Func<Team, bool>(x => x.Id == newProject.TeamId))).FirstOrDefault();
                    if (team != null)
                        newProject.Team = team;
                }
                _projects.Update(newProject);
                await _projects.SaveAsync(token.Token);
                _queueService.Send($"Project {newProject} was updated");
            }
        }

        public async Task<IEnumerable<(DTOProject Project, int TaskCount)>> GetProjectTasksCountByUserIdAsync(int id)
        {
            var tasks = await _tasks.GetAsync();
            var projects = await _projects.GetAsync();

            return tasks.Where(t => t.PerformerId == id)
                .Join(projects, x => x.ProjectId, y => y.Id, (x,y) => new Model.Model.Task()
                {
                    Id = x.Id,
                    CreatedAt = x.CreatedAt,
                    Description = x.Description, 
                    Project = y,
                    ProjectId = x.ProjectId,
                    FinishedAt = x.FinishedAt,
                    Name = x.Name,
                    PerformerId = x.PerformerId,
                    StateId = x.StateId
                })
                .GroupBy(x => x.Project)
                .Select(x => (Project: _mapper.Map<Project, DTOProject>(x.Key), TaskCount: x.Count()));
        }

        public async Task<(DTOUser User, DTOProject LastProject, int CanceledTasksCount, DTOTask LongestTask, int LastProjectUserTasks)> GetUserInfoById(int id)
        {
            var states = await _states.GetAsync();
            var user = (await _users.GetAsync()).Where(x => x.Id == id).FirstOrDefault();
            var lastProject = (await _projects.GetAsync()).OrderBy(x => x.CreatedAt).LastOrDefault();
            var tasks = (await _tasks.GetAsync()).Where(t => t.PerformerId == id)
                .Select(t =>
                    {
                        t.State = states.FirstOrDefault(x => x.Id == t.StateId);
                        return t;
                    });
            return
                (
                    User: _mapper.Map<User, DTOUser>(user),
                    LastProject: _mapper.Map<Project, DTOProject>(lastProject),
                    CanceledTasksCount: tasks.Count(x => x.State.Value == "Canceled" || x.State.Value == "Started"),
                    LongestTask: _mapper.Map<Model.Model.Task, DTOTask>(tasks.OrderByDescending(x => x.FinishedAt - x.CreatedAt).FirstOrDefault()),
                    LastProjectUserTasks: tasks.Count(x => x.ProjectId == lastProject.Id)
                );
        }

        public async Task<(DTOProject Project, DTOTask LongestTask, DTOTask ShortestTask, int UsersCount)> GetProjectInfoById(int id)
        {
            var project = (await _projects.GetAsync()).Where(x => x.Id == id).FirstOrDefault();
            var projectTasks = (await _tasks.GetAsync()).Where(x => x.ProjectId == id);
            var users = await _users.GetAsync();

            return
                (
                    Project: _mapper.Map<Project, DTOProject>(project),
                    LongestTask: _mapper.Map<Model.Model.Task, DTOTask>(projectTasks.OrderByDescending(t => t.Description).FirstOrDefault()),
                    ShortestTask: _mapper.Map<Model.Model.Task, DTOTask>(projectTasks.OrderBy(t => t.Name).FirstOrDefault()),
                    UsersCount: (projectTasks.Count() < 3 || project.Description.Length > 25 ?
                                                users.Where(x => x?.TeamId == project.TeamId).Count() : -1)
                );
        }
    }
}
