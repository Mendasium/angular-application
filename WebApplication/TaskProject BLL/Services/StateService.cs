﻿using Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccess.Repositories;
using BLL.Services.ServicesAbstraction;
using System.Threading;
using Task = System.Threading.Tasks.Task;
using Model.Model.DTOModel;
using AutoMapper;

namespace BLL.Services
{
    public class StateService : IService<DTOState>
    {
        IRepository<State> _states;
        IQueueService _queueService;
        CancellationTokenSource token;
        IMapper _mapper;

        public StateService(IRepository<State> states, IQueueService queueService, IMapper mapper)
        {
            _queueService = queueService;
            _states = states;
            _queueService = queueService;
            _mapper = mapper;
            token = new CancellationTokenSource();
        }

        public async Task<DTOState> Create(DTOState entity)
        {
            var newState = _mapper.Map<DTOState, State>(entity);

            _states.Create(newState);
            await _states.SaveAsync(token.Token);
            _queueService.Send($"State {newState.ToString()} was created");

            var result = (await _states.GetAsync(x => x.Id == newState.Id)).First();
            return _mapper.Map<State, DTOState>(result);
        }

        public async Task Delete(DTOState entity)
        {
            var newState = _mapper.Map<DTOState, State>(entity);
            _states.Delete(newState);
            await _states.SaveAsync(token.Token);
            _queueService.Send($"State {newState.ToString()} was removed");
        }

        public async Task Delete(int id)
        {
            _states.Delete(id);
            await _states.SaveAsync(token.Token);
            _queueService.Send($"State {id} was removed");
        }

        public async Task<IEnumerable<DTOState>> GetEntities(int id = 0, Func<DTOState, bool> filter = null)
        {
            _queueService.Send($"Getting all States " + (filter == null ? "" : " with filter"));
            if(id > 0)
            {
                var res = await _states.GetAsync(x => x.Id == id);
                return _mapper.Map<IEnumerable<State>, IEnumerable<DTOState>>(res);
            }
            var result = await _states.GetAsync(_mapper.Map<Func<DTOState, bool>, Func<State, bool>>(filter));
            return _mapper.Map<IEnumerable<State>, IEnumerable<DTOState>>(result);
        }

        public async Task Update(DTOState entity)
        {
            var newState = _mapper.Map<DTOState, State>(entity);

            _states.Update(newState);
            await _states.SaveAsync(token.Token);
            _queueService.Send($"State {newState.ToString()} was updated");
        }
    }
}
