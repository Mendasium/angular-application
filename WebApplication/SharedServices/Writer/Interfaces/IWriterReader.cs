﻿using SharedServices.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SharedServices.Writer.Interfaces
{
    public interface IWriterReader<T>
    {
        Task WriteAsync(Message message);
        IEnumerable<T> ReadAll(string path = null);
        Task<IEnumerable<Message>> ReadAll();
    }
}
