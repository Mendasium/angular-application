﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LINQ
{
    public class TaskWorker
    {
        List<Task<string>> currentTasks;
        Timer showDataTimer;
        Random randomizer;

        public TaskWorker()
        {
            currentTasks = new List<Task<string>>();
        }

        public void StartWork(int delay)
        {
            randomizer = new Random(DateTime.Now.Millisecond);
            TimerCallback taskTimer = new TimerCallback(StartTaskFromList);

            showDataTimer = new Timer(taskTimer, currentTasks, delay, delay);
        }

        public void AddTaskToList(Func<Task<string>> action)
        {
            Console.WriteLine("Task was added");
            currentTasks.Add(AddNewTask(action));
        }

        private async Task<string> AddNewTask(Func<Task<string>> function)
        {
            if (function == null)
                throw new ArgumentNullException("No function");

            var tcs = new TaskCompletionSource<string>();

            try
            {
                var result = await function();
                tcs.SetResult(result);
            }
            catch (Exception exc)
            {
                tcs.SetException(exc);
            }

            return await tcs.Task;
        }

        private async void StartTaskFromList(object data)
        {
            if (data is List<Task<string>> tasks)
            {
                if (tasks.Count == 0)
                    return;
                var currentTask = tasks[randomizer.Next(tasks.Count)];
                // currentTask.Start();
                try
                {
                    tasks.Remove(currentTask);
                    await currentTask.ContinueWith((t) =>
                    {
                        if (t.IsFaulted)
                        {
                            Console.WriteLine(t.Exception);
                        }
                        else
                        {
                            Console.WriteLine("Task was completed, it's Id: " + t.Id);
                            Console.WriteLine("Result: " + t.Result);
                        }
                    });
                }
                catch (AggregateException)
                {
                    Console.WriteLine("aggredate exception");
                }
                catch (InvalidOperationException)
                {
                    Console.WriteLine("invalid Operation exception");
                }
            }
        }

    }
}
