﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using LINQ.Model;
using Newtonsoft.Json;
using Task = LINQ.Model.Task;
using LINQ.Model.SpecialClasses;

namespace LINQ
{
    class ProjectsService
    {
        private static HttpClient Client = new HttpClient();

        private readonly string url;

        public ProjectsService(string apiUrl)
        {
            url = apiUrl + "/api/";
        }

        #region RemoveData
        public async Task<string> Remove(string dataType, int id)
        {
            try
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Delete, url + dataType + "/" + id);
                HttpResponseMessage response = await Client.SendAsync(request);
                response.EnsureSuccessStatusCode();

                return await response.Content.ReadAsStringAsync();
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine($"The error was caught:\n {e.Message}");
                return string.Empty;
            }
        }
        #endregion

        #region GetData
        public async Task<string> GetData(string urlEnding)
        {
            try
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, url + urlEnding);
                HttpResponseMessage response = await Client.SendAsync(request);
                response.EnsureSuccessStatusCode();

                return await response.Content.ReadAsStringAsync();
            }
            catch (HttpRequestException e)
            {
                throw new HttpRequestException(e.Message);
                Console.WriteLine($"The error was caught:\n {e.Message}");
                return null;
            }
        }

        public async Task<IEnumerable<string>> GetMessages()
        {
            var result = GetData("messages");
            if (result == null)
                return new List<string>();
            return JsonConvert.DeserializeObject<IEnumerable<string>>(await result);
        }
        public async Task<IEnumerable<State>> GetStates()
        {
            var result = GetData("state");
            if (result == null)
                return new List<State>();
            return JsonConvert.DeserializeObject<IEnumerable<State>>(await result);
        }
        public async Task<IEnumerable<Project>> GetProjects()
        {
            var result = GetData("project");
            if (result == null)
                return new List<Project>();
            return JsonConvert.DeserializeObject<IEnumerable<Project>>(await result);
        }
        public async Task<IEnumerable<Team>> GetTeams()
        {
            var result = GetData("team");
            if (result == null)
                return new List<Team>();
            return JsonConvert.DeserializeObject<IEnumerable<Team>>(await result);
        }
        public async Task<IEnumerable<User>> GetUsers()
        {
            var result = GetData("user");
            if (result == null)
                return new List<User>();
            return JsonConvert.DeserializeObject<IEnumerable<User>>(await result);
        }
        public async Task<IEnumerable<Task>> GetTasks()
        {
            var result = GetData("task");
            if (result == null)
                return new List<Task>();
            return JsonConvert.DeserializeObject<IEnumerable<Task>>(await result);
        }
        #endregion

        #region AddUpdateData
        public async Task<string> AddData(string dataType, dynamic data)
        {
            try
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, url + dataType);
                var content = JsonConvert.SerializeObject(data);
                request.Content = new StringContent(content, Encoding.UTF8, "application/json");
                var result = await Client.SendAsync(request);

                return await result.Content.ReadAsStringAsync();
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine($"Smth get's wrong {e.Message}");
                return null;
            }
        }
        public async Task<string> UpdateData(string dataType, dynamic data)
        {
            try
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Put, url + dataType);
                var content = JsonConvert.SerializeObject(data);
                request.Content = new StringContent(content, Encoding.UTF8, "application/json");
                var result = await Client.SendAsync(request);

                return await result.Content.ReadAsStringAsync();
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine($"Smth get's wrong {e.Message}");
                return string.Empty;
            }
        }
        #endregion

        #region LINQRequests
        public async Task<IEnumerable<(Project Project, int TaskCount)>> GetProjectTasksCountByUserId(int id)
        {
            var result = GetData("project/projectTasks/" + id);
            if (result == null)
                throw new Exception($"Smth goes wrong");
            return JsonConvert.DeserializeObject<IEnumerable<(Project Project, int TaskCount)>>(await result);
        }

        public async Task<IEnumerable<STask>> GetShortnameTasksByPerformerIdByUserId(int id)
        {
            var result = GetData("task/shortNameTasks/" + id);
            if (result == null)
                return new List<STask>();
            return JsonConvert.DeserializeObject<IEnumerable<STask>>(await result);
        }

        public async Task<IEnumerable<(int Id, string Name)>> GetFinishedTasksByUserId(int id, int year = 2019)
        {
            var result = GetData("task/finishedTasks/" + id + "/" + year);
            if (result == null)
                return new List<(int, string)>();
            return JsonConvert.DeserializeObject<IEnumerable<(int Id, string Name)>>(await result);
        }

        public async Task<IEnumerable<(int id, string name, IEnumerable<User> userList)>> GetSortedTeams()
        {
            var result = GetData("user/getSortedTeams");
            if (result == null)
                return new List<(int, string, IEnumerable<User>)>();
            return JsonConvert.DeserializeObject<IEnumerable<(int id, string name, IEnumerable<User> userList)>>(await result);
        }

        public async Task<IEnumerable<(Task user, IEnumerable<Task> tasks)>> GetSortedUsersWithTasks()
        {
            var result = GetData("task/sortedBusyUsers");
            if (result == null)
                return new List<(Task, IEnumerable<Task>)>();
            return JsonConvert.DeserializeObject<IEnumerable<(Task user, IEnumerable<Task> tasks)>>(await result);
        }

        //smthh wrong
        public async Task<(User User, Project LastProject, int CanceledTasksCount, Task LongestTask, int LastProjectUserTasks)> GetUserInfoById(int id)
        {
            var result = GetData("project/userInfo/" + id);
            if (result == null)
                throw new Exception($"Smth goes wrong");
            return JsonConvert.DeserializeObject<(User User, Project LastProject, int CanceledTasksCount, Task LongestTask, int LastProjectUserTasks)>(await result);
        }

        public async Task<(Project Project, Task LongestTask, Task ShortestTask, int UsersCount)> GetProjectInfoById(int id)
        {
            var result = GetData("project/projectInfo/" + id);
            if (result == null)
                throw new Exception($"Smth goes wrong");
            return JsonConvert.DeserializeObject<(Project Project, Task LongestTask, Task ShortestTask, int UsersCount)>(await result);
        }
        #endregion
    }
}
