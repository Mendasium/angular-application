﻿using LINQ.Model;
using Microsoft.AspNetCore.SignalR.Client;
using Newtonsoft.Json;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LINQ
{
    class Program
    {
        static readonly string hubUrl = "https://localhost:44343/answers";
        static readonly string apiUrl = "https://localhost:44343";

        private static ProjectInterface projectInterface;
        private static HubService hubService;
        

        static void Main(string[] args)
        {
            hubService = new HubService(hubUrl);
            hubService.StartConnection();
            
            projectInterface = new ProjectInterface(apiUrl);
            projectInterface.Start();
        }
    }
}
