﻿using DataAccess;
using Microsoft.EntityFrameworkCore;
using Model.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace TaskProject_BLL_Tests.Fake
{
    public class FakeDbContext
    {
        TaskProjectDBContext dbContext;

        public FakeDbContext(string name)
        {
            dbContext = new TaskProjectDBContext(GetDbContextOptions(name));
        }

        public FakeDbContext InitializeWithData()
        {
            var users = new List<User>()
            {
                new User() { Id = 11, TeamId = 12, FirstName = "Ivan", RegisteredAt = DateTime.Now, Birthday = DateTime.Now.AddYears(-12) },
                new User() { Id = 12, TeamId = 11, FirstName = "Oleksii", RegisteredAt = DateTime.Now, Birthday = DateTime.Now.AddYears(-20) },
                new User() { Id = 13, TeamId = 13, FirstName = "Aleksandr", RegisteredAt = DateTime.Now, Birthday = DateTime.Now.AddYears(-19) },
                new User() { Id = 14, TeamId = 12, FirstName = "Aleksandr", RegisteredAt = DateTime.Now, Birthday = DateTime.Now.AddYears(-13) },
                new User() { Id = 15, TeamId = 11, FirstName = "Aleksandr", RegisteredAt = DateTime.Now, Birthday = DateTime.Now.AddYears(-11) },
            };
            var teams = new List<Team>()
            {
                new Team() { Id = 11, CreatedAt = DateTime.Now, Name = "Team1" },
                new Team() { Id = 12, CreatedAt = DateTime.Now, Name = "Team2" },
                new Team() { Id = 13, CreatedAt = DateTime.Now, Name = "Team3" },
            };
            var tasks = new List<Task>()
            {
                new Task() { Id = 11, Name = "Task1", PerformerId = 1, StateId = 2, ProjectId = 2, Description = "Very long text" },
                new Task() { Id = 12, Name = "Task2", PerformerId = 2, StateId = 3, ProjectId = 1, Description = "Very long text" }
            };
            
            dbContext.Users.AddRange(users);
            dbContext.Teams.AddRange(teams);
            dbContext.Tasks.AddRange(tasks);

            dbContext.SaveChanges();

            return this;
        }

        public TaskProjectDBContext DbContext => dbContext;

        public DbContextOptions<TaskProjectDBContext> GetDbContextOptions(string name)
        {
            var options = new DbContextOptionsBuilder<TaskProjectDBContext>()
                .UseInMemoryDatabase(name)
                .Options;
            return options;
        }
    }
}
