﻿using AutoMapper;
using BLL.Services;
using BLL.Services.ServicesAbstraction;
using DataAccess;
using DataAccess.Repositories;
using Model.Model;
using Model.Model.DTOModel;
using Moq;
using System;
using System.Linq;
using TaskProject.StartupConfiguration;
using TaskProject_BLL_Tests.Fake;
using Xunit;


namespace TaskProject_BLL_Tests.IntegrationTests
{
    public class UserServiceIntTests
    {
        [Fact]
        public async void Remove_When_SuchUserExists_Then_CheckWhatItIsNotAlive()
        {
            var userId = 12;

            using (TaskProjectDBContext dBContext = new FakeDbContext("Users_Remove").InitializeWithData().DbContext)
            {
                IMapper mapper = MapperConfig.ConfigureMapping();
                var userRep = new Repository<User>(dBContext);
                var teamRep = new Repository<Team>(dBContext);
                var queueMock = new Mock<IQueueService>();

                var userService = new UserService(userRep, teamRep, queueMock.Object, mapper);

                var inData = await userService.GetEntities();
                
                await userService.Delete(userId);
                var result = await userService.GetEntities();

                Assert.NotNull(inData.FirstOrDefault(x => x.Id == userId));
                Assert.Null(result.FirstOrDefault(x => x.Id == userId));
            }
        }

        [Fact]
        public async void GetSortedTeams_When_SuchSuchTeamsExists_Then_CheckDataForCorrectValues()
        {
            using (TaskProjectDBContext dBContext = new FakeDbContext("Users_getSortedTeams").InitializeWithData().DbContext)
            {
                IMapper mapper = MapperConfig.ConfigureMapping();
                var userRep = new Repository<User>(dBContext);
                var teamRep = new Repository<Team>(dBContext);
                var queueMock = new Mock<IQueueService>();

                var userService = new UserService(userRep, teamRep, queueMock.Object, mapper);
               
                var result = (await userService.GetSortedTeams()).ToList();

                Assert.Equal(2, result.Count());
                Assert.Equal(12, result[0].id);
                Assert.Equal(13, result[1].id);
                Assert.Single(result[1].userList);
            }
        }
    }
}
