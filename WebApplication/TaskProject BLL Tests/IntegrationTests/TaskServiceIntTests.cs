﻿using AutoMapper;
using BLL.Services;
using BLL.Services.ServicesAbstraction;
using DataAccess;
using DataAccess.Repositories;
using Model.Model;
using Model.Model.DTOModel;
using Moq;
using System;
using System.Linq;
using TaskProject.StartupConfiguration;
using TaskProject_BLL_Tests.Fake;
using Xunit;

namespace TaskProject_BLL_Tests.IntegrationTests
{
    public class TaskServiceIntTests
    {
        [Fact]
        public async void Remove_When_SuchTaskExists_Then_CheckThatIsNotExists()
        {
            var taskId = 12;
            
            using (TaskProjectDBContext dBContext = new FakeDbContext("Tasks_Remove").InitializeWithData().DbContext)
            {
                IMapper mapper = MapperConfig.ConfigureMapping();
                var taskRep = new Repository<Task>(dBContext);
                var projectRep = new Repository<Project>(dBContext);
                var userRep = new Repository<User>(dBContext);
                var stateRep = new Repository<State>(dBContext);
                var queueMock = new Mock<IQueueService>();

                var taskService = new TaskService(taskRep, userRep, projectRep, stateRep, queueMock.Object, mapper);

                var inData = await taskService.GetEntities();

                Assert.NotNull(inData.FirstOrDefault(x => x.Id == taskId));

                await taskService.Delete(taskId);
                var result = await taskService.GetEntities();

                Assert.Null(result.FirstOrDefault(x => x.Id == taskId));
            }

        }
    }
}
