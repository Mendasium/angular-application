﻿using AutoMapper;
using BLL.Services;
using BLL.Services.ServicesAbstraction;
using DataAccess.Repositories;
using Model.Model;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskProject.StartupConfiguration;
using Xunit;
using Task = Model.Model.Task;

namespace TaskProject_BLL_Tests.Services
{
    public class ProjectServiceTests
    {
        [Fact]
        public async void GetProjectTasksCountByUserIdAsync_When_UserIdIsThree_Then_GetTwoProjectsWithOneAndThreeTasks()
        {
            var tasks = new List<Task>()
            {
                new Task(){ Id = 1, Name = "Task1", PerformerId = 2, ProjectId = 3 },
                new Task(){ Id = 2, Name = "Task2", PerformerId = 3, ProjectId = 3 },
                new Task(){ Id = 3, Name = "Task3", PerformerId = 1, ProjectId = 2 },
                new Task(){ Id = 4, Name = "Task4", PerformerId = 3, ProjectId = 1 },
                new Task(){ Id = 5, Name = "Task5", PerformerId = 3, ProjectId = 1 }
            };
            var projects = new List<Project>()
            {
                new Project(){ Id = 1, Name = "Project1" },
                new Project(){ Id = 2, Name = "Project2" },
                new Project(){ Id = 3, Name = "Project3" }
            };

            IMapper mapper = MapperConfig.ConfigureMapping();


            var taskMock = new Mock<IRepository<Task>>();
            taskMock.Setup(rep => rep.GetAsync(It.IsAny<Func<Task, bool>>()))
                .Returns(() => System.Threading.Tasks.Task.Factory.StartNew(() => tasks.Select(x => x)));

            var projectMock = new Mock<IRepository<Project>>();
            projectMock.Setup(rep => rep.GetAsync(It.IsAny<Func<Project, bool>>()))
                .Returns(() => System.Threading.Tasks.Task.Factory.StartNew(() => projects.Select(x => x)));

            var userMock = new Mock<IRepository<User>>();
            var teamsMock = new Mock<IRepository<Team>>();
            var statesMock = new Mock<IRepository<State>>();
            var queueMock = new Mock<IQueueService>();

            var projectService = new ProjectService(projectMock.Object, userMock.Object, teamsMock.Object, taskMock.Object, statesMock.Object, queueMock.Object, mapper);

            var result = await projectService.GetProjectTasksCountByUserIdAsync(3);

            Assert.Equal(2, result.Count());
            Assert.Equal(3, result.ToList()[0].Project.Id);
            Assert.Equal(1, result.ToList()[0].TaskCount);
            Assert.Equal(1, result.ToList()[1].Project.Id);
            Assert.Equal(2, result.ToList()[1].TaskCount);
        }

        [Fact]
        public async void GetUserInfoById_When_UserIdIsThree_Then_GetCorrectStructure()
        {
            var tasks = new List<Task>()
            {
                new Task(){ Id = 1, Name = "Task1", PerformerId = 2, ProjectId = 3, StateId = 1, FinishedAt = DateTime.Now, CreatedAt = DateTime.Now.AddDays(-120) },
                new Task(){ Id = 2, Name = "Task2", PerformerId = 3, ProjectId = 3, StateId = 3, FinishedAt = DateTime.Now, CreatedAt = DateTime.Now.AddDays(-200) },
                new Task(){ Id = 3, Name = "Task3", PerformerId = 1, ProjectId = 2, StateId = 2, FinishedAt = DateTime.Now, CreatedAt = DateTime.Now.AddDays(-300) },
                new Task(){ Id = 4, Name = "Task4", PerformerId = 3, ProjectId = 1, StateId = 1, FinishedAt = DateTime.Now, CreatedAt = DateTime.Now.AddDays(-400) },
                new Task(){ Id = 5, Name = "Task5", PerformerId = 3, ProjectId = 3, StateId = 2, FinishedAt = DateTime.Now, CreatedAt = DateTime.Now.AddDays(-100) }
            };
            var projects = new List<Project>()
            {
                new Project(){ Id = 1, Name = "Project1", CreatedAt = DateTime.Now.AddYears(-2) },
                new Project(){ Id = 2, Name = "Project2", CreatedAt = DateTime.Now.AddYears(-3) },
                new Project(){ Id = 3, Name = "Project3", CreatedAt = DateTime.Now.AddYears(-1) }
            };
            var users = new List<User>()
            {
                new User(){ Id = 1, FirstName = "Ivan" },
                new User(){ Id = 2, FirstName = "Sergii" },
                new User(){ Id = 3, FirstName = "Oleksii" },
            };

            IMapper mapper = MapperConfig.ConfigureMapping();


            var taskMock = new Mock<IRepository<Task>>();
            taskMock.Setup(rep => rep.GetAsync(It.IsAny<Func<Task, bool>>()))
                .Returns(() => System.Threading.Tasks.Task.Factory.StartNew(() => tasks.Select(x => x)));

            var projectMock = new Mock<IRepository<Project>>();
            projectMock.Setup(rep => rep.GetAsync(It.IsAny<Func<Project, bool>>()))
                .Returns(() => System.Threading.Tasks.Task.Factory.StartNew(() => projects.Select(x => x)));

            var userMock = new Mock<IRepository<User>>();
            userMock.Setup(rep => rep.GetAsync(It.IsAny<Func<User, bool>>()))
                .Returns(() => System.Threading.Tasks.Task.Factory.StartNew(() => users.Select(x => x)));

            var statesMock = new Mock<IRepository<State>>();
            statesMock.Setup(rep => rep.GetAsync(It.IsAny<Func<State, bool>>()))
                .Returns(ReturnStates());

            var teamsMock = new Mock<IRepository<Team>>();
            var queueMock = new Mock<IQueueService>();

            var projectService = new ProjectService(projectMock.Object, userMock.Object, teamsMock.Object, taskMock.Object, statesMock.Object, queueMock.Object, mapper);

            var result = await projectService.GetUserInfoById(3);

            Assert.Equal(3, result.User.Id);
            Assert.Equal(4, result.LongestTask.Id);
            Assert.Equal(3, result.LastProject.Id);
            Assert.Equal(2, result.LastProjectUserTasks);
            Assert.Equal(2, result.CanceledTasksCount);
        }
        private async Task<IEnumerable<State>> ReturnStates()
        {
            return await System.Threading.Tasks.Task.Factory.StartNew(() => new List<State>()
            {
                new State() { Id = 1, Value = "Done" },
                new State() { Id = 2, Value = "Started" },
                new State() { Id = 3, Value = "Canceled" }
            });
        }

        [Fact]
        public async void GetProjectInfoById_When_ProjectIdIsThree_Then_GetCorrectStructure()
        {
            var tasks = new List<Task>()
            {
                new Task(){ Id = 1, Name = "Task12", ProjectId = 3, Description = "1234" },
                new Task(){ Id = 2, Name = "Task234", ProjectId = 3, Description = "1234567890" },
                new Task(){ Id = 3, Name = "Task3", ProjectId = 2, Description = "12" },
                new Task(){ Id = 4, Name = "Task456", ProjectId = 1, Description = "1234567" },
                new Task(){ Id = 5, Name = "Task5432", ProjectId = 3, Description = "123456" }
            };
            var projects = new List<Project>()
            {
                new Project(){ Id = 1, Name = "Project1", CreatedAt = DateTime.Now.AddYears(-2), Description = "Nothing more" },
                new Project(){ Id = 2, Name = "Project2", CreatedAt = DateTime.Now.AddYears(-3), Description = "Something very long" },
                new Project(){ Id = 3, Name = "Project3", CreatedAt = DateTime.Now.AddYears(-1), Description = "Something very very very long. Here needs to be 25 symbols", TeamId = 2 }
            };
            var users = new List<User>()
            {
                new User(){ Id = 1, FirstName = "Ivan" },
                new User(){ Id = 2, FirstName = "Sergii", TeamId = 2 },
                new User(){ Id = 3, FirstName = "Oleksii", TeamId = 2 },
            };

            IMapper mapper = MapperConfig.ConfigureMapping();


            var taskMock = new Mock<IRepository<Task>>();
            taskMock.Setup(rep => rep.GetAsync(It.IsAny<Func<Task, bool>>()))
                .Returns(() => System.Threading.Tasks.Task.Factory.StartNew(() => tasks.Select(x => x)));

            var projectMock = new Mock<IRepository<Project>>();
            projectMock.Setup(rep => rep.GetAsync(It.IsAny<Func<Project, bool>>()))
                .Returns(() => System.Threading.Tasks.Task.Factory.StartNew(() => projects.Select(x => x)));

            var userMock = new Mock<IRepository<User>>();
            userMock.Setup(rep => rep.GetAsync(It.IsAny<Func<User, bool>>()))
                .Returns(() => System.Threading.Tasks.Task.Factory.StartNew(() => users.Select(x => x)));

            var statesMock = new Mock<IRepository<State>>();
            var teamsMock = new Mock<IRepository<Team>>();
            var queueMock = new Mock<IQueueService>();

            var projectService = new ProjectService(projectMock.Object, userMock.Object, teamsMock.Object, taskMock.Object, statesMock.Object, queueMock.Object, mapper);

            var result = await projectService.GetProjectInfoById(3);

            Assert.Equal(3, result.Project.Id);
            Assert.Equal(2, result.LongestTask.Id);
            Assert.Equal(1, result.ShortestTask.Id);
            Assert.Equal(2, result.UsersCount);
        }
    }
}
