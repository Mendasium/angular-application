﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Model.Model.DTOModel
{
    public class DTOState : DTOEntity
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}
