﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Model
{
    public class Project : Entity
    {
        [Required]
        [MaxLength(20)]
        public string Name { get; set; }
        public string Description { get; set; }
        [Required]
        public DateTime CreatedAt { get; set; }
        [Required]
        public DateTime Deadline { get; set; }
        public User Author { get; set; }
        public Team Team { get; set; }

        public int? AuthorId { get; set; }
        public int? TeamId { get; set; }

        public Project() { }

        public override string ToString()
        {
            return $"{Id} - {Name} - {Description}";
        }

        public override void Update(Entity entity)
        {
            if(entity is Project project)
            {
                if (!string.IsNullOrEmpty(project.Name) && project.Name.Length > 3 && project.Name != Name)
                    this.Name = project.Name;
                if (!string.IsNullOrEmpty(project.Description) && project.Description.Length > 3 && project.Description != Description)
                    this.Description = project.Description;
                if (Deadline != project.Deadline && project.Deadline != new DateTime())
                    this.Deadline = project.Deadline;
                if (project.Author != null)
                    Author = project.Author;
                if (project.Team != null) 
                    Team = project.Team;
                AuthorId = Author?.Id;
                TeamId = Team?.Id;
            }
            else
            {
                throw new FormatException("You need to use project entity here");
            }
        }
    }
}
