﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QueueService.Model
{
    public class MessageScopedSettings
    {
        public string ExchangeName { get; set; }

        public string QueueName { get; set; }

        public string RoutingKey { get; set; }

        public string ExchangeType { get; set; }
    }
}
