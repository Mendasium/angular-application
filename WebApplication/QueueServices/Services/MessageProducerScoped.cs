﻿using QueueService.Model;
using QueueServices.Abstractions;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace QueueServices.Services
{
    public class MessageProducerScoped : IMessageProducerScoped
    {
        private readonly Lazy<IMessageQueue> _messageQueue;
        private readonly Lazy<IMessageProducer> _messageProducer;

        private readonly MessageScopedSettings _messageScopedSettings;
        private readonly IConnectionFactory _connectionFactory;

        public MessageProducerScoped(IConnectionFactory connectionFactory, MessageScopedSettings messageScopedSettings)
        {
            _messageScopedSettings = messageScopedSettings;
            _connectionFactory = connectionFactory;

            _messageProducer = new Lazy<IMessageProducer>(() => CreateMessageProducer());
            _messageQueue = new Lazy<IMessageQueue>(() => new MessageQueue(connectionFactory, messageScopedSettings));
        }

        public IMessageProducer MessageProducer => _messageProducer.Value;

        private IMessageQueue MessageQueue => _messageQueue.Value;

        private IMessageQueue CreateMessageQueue()
        {
            return new MessageQueue(_connectionFactory, _messageScopedSettings);
        }

        private IMessageProducer CreateMessageProducer()
        {
            return new MessageProducer(new MessageProducerSettings()
            {
                Channel = MessageQueue.Channel,
                PublicationAddress = new PublicationAddress(
                    _messageScopedSettings.ExchangeType,
                    _messageScopedSettings.ExchangeName,
                    _messageScopedSettings.RoutingKey)
            });
        }

        public void Dispose()
        {
            MessageQueue?.Dispose();
        }

    }
}
