﻿using QueueService.Model;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;
using QueueServices.Abstractions;

namespace QueueServices.Services
{
    public class MessageConsumerScopedFactory : IMessageConsumerScopedFactory
    {
        private readonly IConnectionFactory _connectionFactory;

        public MessageConsumerScopedFactory(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public IMessageConsumerScoped Open(MessageScopedSettings messageScopedSettings)
        {
            return new MessageConsumerScoped(_connectionFactory, messageScopedSettings);
        }

        public IMessageConsumerScoped Connect(MessageScopedSettings messageScopedSettings)
        {
            var mqConsumerScope = Open(messageScopedSettings);
            mqConsumerScope.MessageConsumer.Connect();

            return mqConsumerScope;
        }
    }
}
