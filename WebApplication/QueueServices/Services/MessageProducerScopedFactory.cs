﻿using QueueService.Model;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;
using QueueServices.Abstractions;

namespace QueueServices.Services
{
    public class MessageProducerScopedFactory : IMessageProducerScopedFactory
    {
        private readonly IConnectionFactory _connectionFactory;

        public MessageProducerScopedFactory(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public IMessageProducerScoped Open(MessageScopedSettings setting)
        {
            return new MessageProducerScoped(_connectionFactory, setting);
        }
    }
}
