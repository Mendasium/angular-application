﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QueueServices.Abstractions
{
    public interface IMessageProducerScoped
    {
        IMessageProducer MessageProducer { get; }
        void Dispose();
    }
}
